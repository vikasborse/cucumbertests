    package autoapp.automation.stepDef;

    import autoapp.automation.pages.navigation.NavigationPage;
    import cucumber.api.DataTable;
    import cucumber.api.java.en.Then;
    import java.util.List;
    import java.util.Map;

    public class NavigationStepDef {


        @Then("^I verify the mega menu is working$")
        public void iVerifyTheMegaMenuIsWorking(DataTable dataTable) {
            List<Map<String,String>> megaMenus = dataTable.asMaps(String.class,String.class);
            // Add all provided products to the cart from Quick View
            for(Map menu : megaMenus) {
                NavigationPage.verifyMegaMenuIsWorking(menu);
            }
        }



    }
